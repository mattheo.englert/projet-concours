<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="projet.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
   <title>| Reshipi</title>
   <link rel="preconnect" href="https://fonts.googleapis.com">
   <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
   <link href="https://fonts.googleapis.com/css2?family=Julee&display=swap" rel="stylesheet">
</head>

<header>
    <nav class="navbar">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">
          <img src="logo_reshipi.png" class="logo" alt="LOGO">
        </a>
        <figure>
          <blockquote class="blockquote">
            <p>RESHIPI </p>
          </blockquote>
          <figcaption class="blockquote-footer">
           Le meilleur de tes recettes d' <cite title="Source Title">Anime </cite> préférés !
        </figcaption></figure>
        <a class="navbar-brand" href="#">Accueil</a>
        <a class="navbar-brand" href="#">À propos</a>
        <form class="d-flex" role="search">
          <input class="form-control me-2" type="search" placeholder="Rechercher" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">
            <i class="bi bi-search-heart">
              <svg xmlns="http://www.w3.org/2000/svg" class="bi bi-search-heart" fill="currentColor" viewBox="0 0 16 16">
            <path d="M6.5 4.482c1.664-1.673 5.825 1.254 0 5.018-5.825-3.764-1.664-6.69 0-5.018Z"/>
            <path d="M13 6.5a6.471 6.471 0 0 1-1.258 3.844c.04.03.078.062.115.098l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1.007 1.007 0 0 1-.1-.115h.002A6.5 6.5 0 1 1 13 6.5ZM6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11Z"/>
          </svg></i></button>
        </form>
      </div>
    </nav>
  </header>

  <body>
    <form>
      <div class="mb-3">
        <label for="Nom" class="form-label">Nom</label>
        <input type="Nom" class="form-control" id="Nom" aria-describedby="emailHelp">
      </div>
      <div class="mb-3">
        <label for="Prénom" class="form-label">Prénom</label>
        <input type="Prénom" class="form-control" id="Prénom">
        <label for="Mail" class="form-label">Adresse mail</label>
        <input type="Mail" class="form-control" id="Mail" aria-describedby="emailHelp">
      </div> <div class="mb-3">
        <label for="Tel" class="form-label">Numéro de téléphone</label>
      <input type="Tel" class="form-control" id="Tel">
    </div>
    <button type="submit" class="btn btn-primary">Envoyer</button>
    <label for="texte" class="form-label">Votre message</label>
    <input type="texte" class="textarea" id="Text"></div>
  </form>
</body>

  <footer class="footer">   
    <div class="texte">  
    <p>Copyright © 2023</p>     
        <ul>         
        <li><a href="#" class="liens"> Politique de confidentialité </a></li>         
        <li><a href="#" class="liens"> Conditions d'utilisation </a></li>         
        <li><a href="contact.php" class="liens"> Nous contacter </a></li>         
        <li><a href="#" class="liens"> Inscription Newsletter </a></li>         
        <li><a href="valeurs.php" class="liens"> Nos valeurs </a></li>     
    </ul> 
</div>  
</footer>
</html>